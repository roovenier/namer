cd /web/namer/
source /web/namer/.env/bin/activate
git pull -u origin dev
pip install -r requirements/staging.txt
python ./manage.py migrate
python ./manage.py collectstatic --noinput
touch /tmp/dev.getnamer.ru
npm run build:webpack
