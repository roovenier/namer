cd /web/getnamer/
source /web/getnamer/.env/bin/activate
git pull -u origin master
pip install -r requirements/production.txt
python ./manage.py migrate
python ./manage.py collectstatic --noinput
touch /tmp/getnamer.ru
npm run build:production
