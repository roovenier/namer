import React, { Component } from 'react';
import classnames from 'classnames';
import NameView from './ConstructorItems/NameView';
import MaterialView from './ConstructorItems/MaterialView';
import LengthView from './ConstructorItems/LengthView';
import PendantView from './ConstructorItems/PendantView';
import CommentView from './ConstructorItems/CommentView';
import { Link } from 'react-router';

export default class ConstructorView extends Component {
	render() {
		return (
			<div className="col-md-8 col-md-offset-1">
				<p className="title">Выбери свой нэймер!</p>

				<form className="constructor__form" action="" method="post" onSubmit={(e) => this.props.add2Basket(e)}>
					<NameView
						setName={(name) => this.props.setName(name)}
					/>

					<MaterialView
						composition={this.props.composition}
						namer={this.props.namer}
						setMaterial={(material) => this.props.setMaterial(material)}
						setColor={(color) => this.props.setColor(color)}
					/>

					<LengthView
						namer={this.props.namer}
						setLength={(length) => this.props.setLength(length)}
					/>

					<PendantView
						composition={this.props.composition}
						namer={this.props.namer}
						setPendant={(pendant) => this.props.setPendant(pendant)}
					/>

					<CommentView
						setComment={(comment) => this.props.setComment(comment)}
					/>

					<div class="constructor__btns">
						<button className="constructor__submit" type="submit">В корзину</button>

						{(() => {
							if(this.props.basket.items.length > 0) {
								return <Link className="constructor__checkout" to={'/order/'}>Оформить</Link>;
							} else return;
						})()}
					</div>
				</form>
			</div>
		);
	}
}
