import React, { Component } from 'react';
import { Link } from 'react-router';

export default class HeaderView extends Component {
	render() {
		return (
			<div className="header">
				<div className="container">
					{(() => {
						if(this.props.basket.items) {
							return (
								<div className="row">
									<div className="col-md-3">
										<Link className="header__logo" to={'/create/'} />
									</div>

									<div className="col-md-8 col-md-offset-1 translucent">
										<div className="row">
											<div className="col-md-4">
												<p className="header__label">Звоните нам по номеру</p>

												<p className="header__value">+7 (919) 945 4075</p>
											</div>

											<div className="col-md-4">
												<p className="header__label">По вопросам оптовых продаж</p>

												<p className="header__value">sale@getnamer.ru</p>
											</div>

											<div className="col-md-4">
												<div className="basket">
													<div className="basket__namer">
														<p className="basket__quantity">{this.props.basket.items.length}</p>

														<div className="basket__item basket__item-1"></div>

														<div className="basket__item basket__item-2"></div>

														<div className="basket__item basket__item-3"></div>
													</div>

													<div className="basket__info translucent">
														<p className="basket__value">{this.props.basket.commonPrice} ₽</p>

														<Link className="basket__checkout" to={'/order/'}>Оформить заказ</Link>
													</div>
												</div>
											</div>

											<div className="col-md-3 hide">
												<p className="header__name">Мария</p>

												<a className="header__logout" href="javascript:void(0)">Выход</a>
											</div>
										</div>
									</div>
								</div>
							);
						}
					})()}
				</div>
			</div>
		);
	}
}
