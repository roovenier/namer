import React, { Component } from 'react';
import classnames from 'classnames';

export default class NamerItemView extends Component {
	render() {
		let namerClass = classnames({
			'namer__visual': true,
			'namer__visual-list': this.props.type
		});
		return (
			<div className={namerClass} style={{borderColor: this.props.namer.color.hex}}>
				<ul className="namer__props">
					<li className="namer__prop">
						<p className="namer__label translucent">Ремешок</p>

						<p className="namer__value">{this.props.namer.material.name} / {this.props.namer.color.name}</p>
					</li>

					<li className="namer__prop">
						<p className="namer__label translucent">Длина</p>

						<p className="namer__value">{this.props.namer.length} см</p>
					</li>

					<li className="namer__prop">
						<p className="namer__label translucent">Подвеска</p>

						<p className="namer__value">{this.props.namer.pendant.name}</p>
					</li>
				</ul>

				<div className="namer__letters" ref="namer_letters">{this.props.namer.name}</div>
			</div>
		);
	}

	componentDidMount() {
		this.setLetters();
	}

	componentDidUpdate() {
		this.setLetters();
	}

	setLetters() {
		$(this.refs.namer_letters).circleType({radius:112, dir:-1});
	}
}
