import React, { Component } from 'react';
import NamerItemView from './NamerItemView';

export default class NamerView extends Component {
	render() {
		return (
			<div className="col-md-3">
				<div className="namer">
					<NamerItemView
						namer={this.props.namer}
					/>

					<div className="namer__info">
						<p className="namer__price">Стоимость: {this.props.namer.price} ₽</p>

						<p className="namer__label">Без учёта скидок и доставки.</p>

						<button className="namer__share" type="button" onClick={(e) => this.props.shareUrl()}>Поделиться с друзьями</button>

						{(() => {
							if(this.props.namer.shareUrl && this.props.namer.shareUrl.result === 'error') {
								return <p className="namer__error">{this.props.namer.shareUrl.error}</p>;
							} else if(this.props.namer.shareUrl && this.props.namer.shareUrl.result === 'success') {
								return <a className="namer__link translucent" href={this.props.namer.shareUrl.url}>{this.props.namer.shareUrl.url}</a>;
							} else if(this.props.namer.shareUrl === 'loading') {
								return <p className="namer__loading">Подождите...</p>;
							} else return;
						})()}
					</div>
				</div>
			</div>
		);
	}
}
