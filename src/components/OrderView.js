import React, { Component } from 'react';
import ReactDOM from 'react-dom';

export default class OrderView extends Component {
	render() {
		return (
			<div className="order">
				<div className="container">
					<form method="post" action="" onSubmit={(e) => this.onSubmit(e)}>
						<div className="row">
							<div className="col-md-4">
								<p className="order__title">Контакты</p>

								<input className="order__input" type="text" placeholder="Имя" onChange={(e) => this.props.setOrderFirstName(e.target.value)} required />

								<input className="order__input" type="text" placeholder="Фамилия" onChange={(e) => this.props.setOrderLastName(e.target.value)} />

								<input className="order__input" type="text" placeholder="E-mail" onChange={(e) => this.props.setOrderEmail(e.target.value)} required />

								<input className="order__input" type="text" placeholder="Телефон" onChange={(e) => this.props.setOrderTel(e.target.value)} required />
							</div>

							<div className="col-md-4">
								<p className="order__title">Доставка</p>

								<div className="order__options">
									{this.props.composition.deliveryWays.map((item, index) => {
										return (
											<div className="order__option" key={index}>
												<input className="order__radio" id={item.value} type="radio" name="delivery" value={item.value} defaultChecked={item.value == this.props.order.deliveryWay.value} onChange={(e) => this.props.setDeliveryWay(item)} />

												<label className="order__check" htmlFor={item.value}>{item.name}</label>
											</div>
										);
									})}
								</div>

								<input className="order__input" type="text" placeholder="Город" onChange={(e) => this.props.setOrderCity(e.target.value)} />

								<input className="order__input" type="text" placeholder="Адрес" onChange={(e) => this.props.setOrderAddress(e.target.value)} />

								<input className="order__input" type="text" placeholder="Время" onChange={(e) => this.props.setOrderTime(e.target.value)} />
							</div>

							<div className="col-md-4">
								<p className="order__title">Оплата</p>

								<div className="order__options">
									{this.props.composition.paymentWays.map((item, index) => {
										return (
											<div className="order__option" key={index}>
												<input className="order__radio" id={item.value} type="radio" name="payment" value={item.value} defaultChecked={item.value == this.props.order.paymentWay.value} onChange={(e) => this.props.setPaymentWay(item)} />

												<label className="order__check" htmlFor={item.value}>{item.name}</label>
											</div>
										);
									})}
								</div>

								{(() => {
									if(this.props.order.promocode.result && this.props.order.promocode.result === 'error') {
										return (
											<p className="order__promo_err">{this.props.order.promocode.error}</p>
										);
									} else return;
								})()}

								<div className="order__promo">
									<input className="order__input order__input-promo" type="text" placeholder="Промо-код" onChange={(e) => this.props.setOrderPromocode(e.target.value)} />

									{(() => {
										if(this.props.order.promocode.result && this.props.order.promocode.result === 'success') {
											return (
												<span className="order__discount">-{this.props.order.promocode.percent}%</span>
											);
										} else return;
									})()}
								</div>

								<div className="order__total">
									<p className="order__label">Заказ:</p>

									<p className="order__value">
										<span className="order__price">
											{(() => {
												var commonPrice = this.props.basket.commonPrice;
												if(this.props.order.promocode.result && this.props.order.promocode.result === 'success') {
													commonPrice -= (commonPrice * this.props.order.promocode.percent / 100);
												}
												return commonPrice;
											})()} ₽
										</span>


										{(() => {
											if(this.props.order.promocode.result && this.props.order.promocode.result === 'success') {
												return (
													<span className="order__price order__price-old">{this.props.basket.commonPrice} ₽</span>
												);
											} else return;
										})()}
									</p>
								</div>

								<div className="order__total">
									<p className="order__label">Доставка:</p>

									<p className="order__value">
										<span className="order__price">{this.props.order.deliveryWay.price} ₽</span>
									</p>
								</div>

								<div className="order__total">
									<p className="order__label">Общая сумма:</p>

									<p className="order__value">
										<span className="order__price" ref="totalPrice">
											{(() => {
												var commonPrice = this.props.basket.commonPrice;
												if(this.props.order.promocode.result && this.props.order.promocode.result === 'success') {
													commonPrice -= (commonPrice * this.props.order.promocode.percent / 100);
												}
												return commonPrice + this.props.order.deliveryWay.price;
											})()}
										</span>

										<span> ₽</span>
									</p>
								</div>
							</div>
						</div>

						<button className="order__buy" type="submit">Купить</button>
					</form>
				</div>
			</div>
		);
	}

	shouldComponentUpdate(nextProps) {
		return nextProps.order.totalPrice !== ReactDOM.findDOMNode(this.refs.totalPrice).innerHTML || nextProps.order.deliveryWay !== this.props.order.deliveryWay || nextProps.basket.commonPrice !== this.props.basket.commonPrice || nextProps.order.promocode !== this.props.order.promocode;
	}

	componentDidUpdate() {
		this.setOrderTotalPrice(ReactDOM.findDOMNode(this.refs.totalPrice).innerHTML);
	}

	componentDidMount() {
		this.setOrderTotalPrice(ReactDOM.findDOMNode(this.refs.totalPrice).innerHTML);
	}

	setOrderTotalPrice(totalPrice) {
		this.props.setOrderTotalPrice(totalPrice);
	}

	onSubmit(e) {
		e.preventDefault();
		this.props.orderSumbit();
	}
}
