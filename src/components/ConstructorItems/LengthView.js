import React, { Component } from 'react';

export default class LengthView extends Component {
	render() {
		return (
			<div className="constructor__item">
				<p className="constructor__num">3</p>

				<div className="constructor__content">
					<p className="constructor__label">Длина ремешка:</p>

					<div className="constructor__length" ref="constr_length"></div>

					<input type="hidden" name="length" value="" />

					<a className="constructor__guide translucent" href="javascript:void(0)">
						Как определить нужную
						<br />
						длину ремешка?
					</a>
				</div>
			</div>
		);
	}

	componentDidMount() {
		var namerLength = this.refs.constr_length;

		noUiSlider.create(namerLength, {
			start: [ this.props.namer.length ],
			step: 1,
			range: {
				'min': [ 9 ],
				'max': [ 23 ]
			},
			tooltips: wNumb({
				decimals: 0,
				postfix: ' см'
			})
		});

		namerLength.noUiSlider.on('update', (value, handle) => {
			this.props.setLength(wNumb({decimals: 0}).to(parseInt(value[0])));
		});
	}
}
