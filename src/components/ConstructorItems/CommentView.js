import React, { Component } from 'react';

export default class CommentView extends Component {
	render() {
		return (
			<div className="constructor__item">
				<p className="constructor__num">5</p>

				<div className="constructor__content">
					<p className="constructor__label">Комментарий:</p>

					<textarea className="constructor__textarea" name="comment" rows="1" cols="1" placeholder="Если Ваш заказ требует уточнений – оставьте нам комментарий." onChange={(e) => this.props.setComment(e.target.value)}></textarea>
				</div>
			</div>
		);
	}
}
