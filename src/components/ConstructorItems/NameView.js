import React, { Component } from 'react';

export default class NameView extends Component {
	render() {
		return (
			<div className="constructor__item">
				<p className="constructor__num">1</p>

				<div className="constructor__content">
					<input className="constructor__input" type="text" name="name" placeholder="namer" maxLength="13" autoComplete="off" onChange={(e) => this.props.setName(e.target.value)} onKeyPress={(e) => this.keyPress(e)} />

					<p className="constructor__label">заполняется только латинскими буквами, максимальная длина 13 букв</p>
				</div>
			</div>
		);
	}

	keyPress(e) {
		if(!((e.which >= 65 && e.which  <= 90) || (e.which >= 97 && e.which <= 122))) {
			e.preventDefault();
		}
	}
}
