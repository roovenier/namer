import React, { Component } from 'react';
import classnames from 'classnames';
import ColorsView from './ColorsView.js';

export default class MaterialView extends Component {
	render() {
		return (
			<div className="constructor__item">
				<p className="constructor__num">2</p>

				<div className="constructor__content">
					<p className="constructor__label">Выбери цвет и материал ремешка:</p>

					<div className="constructor__props">
						{this.props.composition.materials.map((material, index) => {
							var materialClass = classnames({
								'constructor__prop': true,
								'constructor__prop-active': this.props.namer.material.name == material.name
							});
							return <button className={materialClass} type="button" key={index} onClick={(e) => this.props.setMaterial(material)}>{material.name}</button>
						})}
					</div>

					<input type="hidden" name="material" value="" />

					{(() => {
						if(Object.keys(this.props.namer.material).length) {
							return (
								<ColorsView
									namer={this.props.namer}
									colors={this.props.namer.material.colors}
									setColor={(color) => this.props.setColor(color)}
								/>
							);
						}
					})()}

					<input type="hidden" name="color" value="" />
				</div>
			</div>
		);
	}
}
