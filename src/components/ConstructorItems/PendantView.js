import React, { Component } from 'react';
import classnames from 'classnames';

export default class PendantView extends Component {
	render() {
		return (
			<div className="constructor__item">
				<p className="constructor__num">4</p>

				<div className="constructor__content">
					<p className="constructor__label">Подвеска:</p>

					<div className="constructor__props">
						{this.props.composition.pendants.map((pendant, index) => {
							var pendantClass = classnames({
								'constructor__prop': true,
								'constructor__prop-active': this.props.namer.pendant.name == pendant.name
							});
							return <button className={pendantClass} type="button" key={index} onClick={(e) => this.props.setPendant(pendant)}>{pendant.name}</button>
						})}
					</div>

					<input type="hidden" name="pendant" value="" />
				</div>
			</div>
		);
	}
}
