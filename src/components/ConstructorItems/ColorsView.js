import React, { Component } from 'react';
import classnames from 'classnames';

export default class ColorsView extends Component {
	render() {
		return (
			<div className="constructor__colors">
				{this.props.colors.map((color, index) => {
					var colorClass = classnames({
						'constructor__color': true,
						'constructor__color-active': this.props.namer.color.name == color.name
					});
					return <button className={colorClass} style={{backgroundColor: color.hex}} type="button" key={index} onClick={(e) => this.props.setColor(color)}></button>
				})}
			</div>
		);
	}

	componentDidUpdate() {
		var self = this;
		if(!this.containsColor(this.props.namer.color, this.props.colors)) {
			this.props.setColor(this.props.colors[0])
		}
	}

	containsColor(currColor, colors) {
		for (var i = 0; i < colors.length; i++) {
			if (colors[i] === currColor) {
				return true;
			}
		}
		return false;
	}
}
