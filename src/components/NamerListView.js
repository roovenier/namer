import React, { Component } from 'react';
import NamerItemView from './NamerItemView';
import { Link } from 'react-router';
import classnames from 'classnames';

export default class NamerListView extends Component {
	render() {
		let colClass = classnames({
			'col-lg-12 col-md-12': this.props.basket.items.length == 1,
			'col-lg-6 col-md-6': this.props.basket.items.length == 2,
			'col-lg-4 col-md-4': this.props.basket.items.length > 2
		});
		return (
			<div className="namer__list">
				<div className="row row-clear">
					{this.props.basket.items.map((item, index) => {
						return (
							<div className={colClass} key={index}>
								<div className="namer__item">
									<NamerItemView
										namer={item.namer}
										type={'list'}
									/>

									<div className="namer__info">
										<p className="namer__price">
											<span>
												{(() => {
													var commonPrice = item.totalPrice;
													if(this.props.order.promocode.result && this.props.order.promocode.result === 'success') {
														commonPrice -= commonPrice / this.props.order.promocode.percent;
													}
													return commonPrice;
												})()} ₽
											</span>

											{(() => {
												if(this.props.order.promocode.result && this.props.order.promocode.result === 'success') {
													return (
														<span className="namer__price-old">{item.totalPrice} ₽</span>
													);
												} else return;
											})()}
										</p>

										<div className="namer__count">
											<button className="namer__btn namer__btn-decrease" onClick={(e) => this.decreaseQuantity(item.quantity, index)}>-</button>

											<p className="namer__quantity">{item.quantity}</p>

											<button className="namer__btn namer__btn-increase" onClick={(e) => this.props.increaseQuantity(index)}>+</button>
										</div>
									</div>
								</div>
							</div>
						);
					})}
				</div>

				<div className="text-center">
					<Link className="namer__add" to={'/create/'}>Добавить ещё нэймер</Link>
				</div>
			</div>
		);
	}

	decreaseQuantity(currCount, pos) {
		if(currCount > 1) {
			this.props.decreaseQuantity(pos);
		}
	}
}
