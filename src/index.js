import React from 'react';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { render } from 'react-dom';
import thunk from 'redux-thunk';
import Wrapper from './containers/Wrapper';
import Constructor from './containers/Constructor';
import Order from './containers/Order';
import namerApp from './reducers/index';

//let store = createStore(namerApp);
const createStoreWithMiddleware = applyMiddleware(thunk)(createStore);
let store = createStoreWithMiddleware(namerApp);

render(
	<Provider store={store}>
		<Router history={browserHistory}>
			<Route path='/' component={Wrapper}>
				<Route name='create' path='create' component={Constructor} />
				<Route name='order' path='order' component={Order} />
			</Route>
		</Router>
	</Provider>,
	document.getElementById('root')
);
