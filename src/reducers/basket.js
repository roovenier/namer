import { SET_BASKET_STATE, UPDATE_COMMON_PRICE, ADD_TO_BASKET, DECREASE_QUANTITY, INCREASE_QUANTITY } from '../actions/basket';

export function basket(state = [], action) {
	switch (action.type) {
		case SET_BASKET_STATE:
			return action.basket;
			break;
		case UPDATE_COMMON_PRICE:
			var commonPrice = 0;
			state.items.map((item, index) => commonPrice += item.totalPrice);
			return Object.assign({}, state, {
				commonPrice
			});
			break;
		case ADD_TO_BASKET:
			var newNamer = {
				namer: action.namer,
				quantity: 1,
				totalPrice: action.namer.price
			};
			return Object.assign({}, state, {
				items: [...state.items, newNamer]
			});
			break;
		case DECREASE_QUANTITY:
			var newState = state.items.slice();
			newState[action.pos].quantity--;
			newState[action.pos].totalPrice = newState[action.pos].quantity * newState[action.pos].namer.price;
			return Object.assign({}, state, {
				items: [...newState]
			});
			break;
		case INCREASE_QUANTITY:
			var newState = state.items.slice();
			newState[action.pos].quantity++;
			newState[action.pos].totalPrice = newState[action.pos].quantity * newState[action.pos].namer.price;
			return Object.assign({}, state, {
				items: [...newState]
			});
			break;
		default:
			return state;
	}
}
