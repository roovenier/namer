import { combineReducers } from 'redux';
import { namer } from './namer';
import { basket } from './basket';
import { composition } from './composition';
import { order } from './order';

const namerApp = combineReducers({
	namer,
	basket,
	composition,
	order
});

export default namerApp;
