import { SET_ORDER_FIRSTNAME, SET_ORDER_LASTNAME, SET_ORDER_EMAIL, SET_ORDER_TEL, SET_ORDER_CITY, SET_ORDER_ADDRESS, SET_ORDER_TIME, SET_PROMOCODE, SET_PROMOCODE_VALUE, SET_DELIVERY_WAY, SET_PAYMENT_WAY, SET_ORDER_TOTAL_PRICE, SET_ORDER_STATE } from '../actions/order';

export function order(state = {}, action) {
	switch (action.type) {
		case SET_ORDER_STATE:
			return Object.assign({}, state, action.state);
		case SET_ORDER_FIRSTNAME:
			return Object.assign({}, state, {
				firstname: action.firstname
			});
		case SET_ORDER_LASTNAME:
			return Object.assign({}, state, {
				lastname: action.lastname
			});
		case SET_ORDER_EMAIL:
			return Object.assign({}, state, {
				email: action.email
			});
		case SET_ORDER_TEL:
			return Object.assign({}, state, {
				tel: action.tel
			});
		case SET_ORDER_CITY:
			return Object.assign({}, state, {
				city: action.city
			});
		case SET_ORDER_ADDRESS:
			return Object.assign({}, state, {
				address: action.address
			});
		case SET_ORDER_TIME:
			return Object.assign({}, state, {
				time: action.time
			});
		case SET_PROMOCODE:
			return Object.assign({}, state, {
				promocode: action.promocode
			});
		case SET_PROMOCODE_VALUE:
			return Object.assign({}, state, {
				promocodeValue: action.promocode.toUpperCase()
			});
		case SET_DELIVERY_WAY:
			return Object.assign({}, state, {
				deliveryWay: action.deliveryWay
			});
		case SET_PAYMENT_WAY:
			return Object.assign({}, state, {
				paymentWay: action.paymentWay
			});
		case SET_ORDER_TOTAL_PRICE:
			return Object.assign({}, state, {
				totalPrice: action.price
			});
		default:
			return state;
	}
}
