import { SET_NAME, SET_MATERIAL, SET_COLOR, SET_LENGTH, SET_PENDANT, SET_BASE_COST, SET_LETTER_COST, SET_STRAP_LENGTH_COST, UPDATE_PRICE, SET_NAMER_STATE, SET_SHARE_LOADING, SET_SHARE_URL_STATE, SET_COMMENT } from '../actions/namer';

export function namer(state = {}, action) {
	switch (action.type) {
		case SET_NAMER_STATE:
			return Object.assign({}, state, action.state);
		case SET_NAME:
			return Object.assign({}, state, {
				name: action.name
			});
		case SET_MATERIAL:
			return Object.assign({}, state, {
				material: action.material
			});
		case SET_COLOR:
			return Object.assign({}, state, {
				color: action.color
			});
		case SET_LENGTH:
			return Object.assign({}, state, {
				length: action.length
			});
		case SET_PENDANT:
			return Object.assign({}, state, {
				pendant: action.pendant
			});
		case SET_BASE_COST:
			return Object.assign({}, state, {
				baseCost: action.baseCost
			});
		case SET_LETTER_COST:
			return Object.assign({}, state, {
				letterCost: action.letterCost
			});
		case SET_STRAP_LENGTH_COST:
			return Object.assign({}, state, {
				strapLengthCost: action.strapLengthCost
			});
		case UPDATE_PRICE:
			return Object.assign({}, state, {
				price: state.baseCost + state.material.price + state.length * state.strapLengthCost + state.pendant.price + state.name.length * state.letterCost
			});
		case SET_SHARE_LOADING:
			return Object.assign({}, state, {
				shareUrl: 'loading'
			});
		case SET_SHARE_URL_STATE:
			return Object.assign({}, state, {
				shareUrl: action.shareUrl
			});
		case SET_COMMENT:
			return Object.assign({}, state, {
				comment: action.comment
			});
		default:
			return state;
	}
}
