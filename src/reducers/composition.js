import { SET_COMPOSITION_STATE } from '../actions/composition';

export function composition(state = {}, action) {
	switch (action.type) {
		case SET_COMPOSITION_STATE:
			return Object.assign({}, state, action.state);
			break;
		default:
			return state;
	}
}
