import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { setDefaultAll } from '../actions/defaults.js';
import HeaderView from '../components/HeaderView';

class Wrapper extends Component {
	constructor(props) {
		super(props);
		this.props.dispatch(setDefaultAll());
	}

	render() {
		return (
			<div>
				<HeaderView
					basket={this.props.basket}
				/>

				{this.props.children}
			</div>
		);
	}
}

function select(state) {
	return {
		basket: state.basket
	};
}

export default connect(select)(Wrapper);
