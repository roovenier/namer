import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { setNameApply, setMaterialApply, setColor, setLengthApply, setPendantApply, setShareLoading, setShareUrl, setComment } from '../actions/namer';
import { addToBasketApply, increaseQuantityApply } from '../actions/basket';
import NamerView from '../components/NamerView';
import ConstructorView from '../components/ConstructorView';

class Constructor extends Component {
	render() {
		const { composition, namer, basket } = this.props;
		return (
			<div className="content">
				<div className="container">
					<div className="constructor">
						{(() => {
							if(composition.materials && composition.pendants && namer.name && namer.material) {
								return (
									<div className="row">
										<NamerView
											namer={namer}
											shareUrl={() => this.shareUrl()}
										/>

										<ConstructorView
											composition={composition}
											namer={namer}
											basket={basket}
											setName={(name) => this.setName(name)}
											setMaterial={(material) => this.props.dispatch(setMaterialApply(material))}
											setColor={(color) => this.props.dispatch(setColor(color))}
											setLength={(length) => this.props.dispatch(setLengthApply(length))}
											setPendant={(pendant) => this.props.dispatch(setPendantApply(pendant))}
											setComment={(comment) => this.props.dispatch(setComment(comment))}
											add2Basket={(e) => this.add2Basket(e)}
										/>
									</div>
								);
							}
						})()}
					</div>
				</div>

				<div ref="constructor-popup" data-remodal-id="constructor">
					<button data-remodal-action="close" className="remodal-close" aria-label="Close"></button>

					<h1>В корзине</h1>

					<p>Нэймер добавлен в корзину!</p>
				</div>

				<div ref="constructor_re-popup" data-remodal-id="constructor_re">
					<button data-remodal-action="close" className="remodal-close" aria-label="Close"></button>

					<h1>В корзине</h1>

					<p>Добавлен еще такой же нэймер</p>
				</div>
			</div>
		);
	}

	shareUrl() {
		this.props.dispatch(setShareLoading());
		const namerObj = {
			name: this.props.namer.name,
			strap_material: this.props.namer.material.id,
			strap_material_color: this.props.namer.color.id,
			strap_length: this.props.namer.length,
			pendant: this.props.namer.pendant.id,
			comment: this.props.namer.comment
		};
		this.props.dispatch(setShareUrl(namerObj));
	}

	setName(name) {
		name = (name.length) ? name.toUpperCase() : 'NAMER';
		this.props.dispatch(setNameApply(name));
	}

	add2Basket(e) {
		e.preventDefault();
		let samePos;
		this.props.basket.items.map(function(item, pos) {
			samePos = (JSON.stringify(item.namer) === JSON.stringify(this.props.namer)) ? pos : -1;
		}.bind(this));
		if(samePos > -1) {
			this.props.dispatch(increaseQuantityApply(samePos));
			$(this.refs['constructor_re-popup']).remodal().open();
		} else {
			this.props.dispatch(addToBasketApply(this.props.namer));
			$(this.refs['constructor-popup']).remodal().open();
		}
	}
}

function select(state) {
	return {
		composition: state.composition,
		namer: state.namer,
		basket: state.basket
	};
}

export default connect(select)(Constructor);
