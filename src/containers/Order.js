import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { decreaseQuantityApply, increaseQuantityApply } from '../actions/basket';
import { setOrderFirstName, setOrderLastName, setOrderEmail, setOrderTel, setOrderCity, setOrderAddress, setOrderTime, setOrderPromocodeApply, setDeliveryWay, setPaymentWay, setOrderTotalPrice, sendOrder } from '../actions/order';
import NamerListView from '../components/NamerListView';
import OrderView from '../components/OrderView';

class Order extends Component {
	render() {
		const { basket, composition, order } = this.props;
		return (
			<div className="content">
				{(() => {
					if(basket.commonPrice != undefined && composition.deliveryWays && composition.paymentWays) {
						return (
							<div>
								<div className="container">
									<p className="title text-center">Ваш заказ:</p>

									<NamerListView
										basket={basket}
										order={order}
										decreaseQuantity={(pos) => this.props.dispatch(decreaseQuantityApply(pos))}
										increaseQuantity={(pos) => this.props.dispatch(increaseQuantityApply(pos))}
									/>
								</div>

								<OrderView
									composition={composition}
									basket={basket}
									order={order}
									setOrderFirstName={(firstname) => this.props.dispatch(setOrderFirstName(firstname))}
									setOrderLastName={(lastname) => this.props.dispatch(setOrderLastName(lastname))}
									setOrderEmail={(email) => this.props.dispatch(setOrderEmail(email))}
									setOrderTel={(tel) => this.props.dispatch(setOrderTel(tel))}
									setOrderCity={(city) => this.props.dispatch(setOrderCity(city))}
									setOrderAddress={(address) => this.props.dispatch(setOrderAddress(address))}
									setOrderTime={(time) => this.props.dispatch(setOrderTime(time))}
									setOrderPromocode={(promocode) => this.props.dispatch(setOrderPromocodeApply(promocode))}
									setDeliveryWay={(deliveryWay) => this.props.dispatch(setDeliveryWay(deliveryWay))}
									setPaymentWay={(paymentWay) => this.props.dispatch(setPaymentWay(paymentWay))}
									setOrderTotalPrice={(price) => this.props.dispatch(setOrderTotalPrice(price))}
									orderSumbit={() => this.orderSumbit()}
								/>
							</div>
						);
					}
				})()}

				<div ref="order-popup" data-remodal-id="order">
					<button data-remodal-action="close" className="remodal-close" aria-label="Close"></button>

					<h1>Поздравляем</h1>

					<p>Ваш заказ принят!</p>
				</div>
			</div>
		);
	}

	orderSumbit() {
		let orderItems = [];
		this.props.basket.items.map((item) => {
			const orderItem = {
				namer: item.namer.name,
				strap_material: item.namer.material.name,
				strap_material_color: item.namer.color.name,
				strap_length: item.namer.length,
				pendant: item.namer.pendant.name,
				comment: item.namer.comment,
				quantity: item.quantity,
				cost: item.totalPrice
			};
			orderItems.push(orderItem);
		});
		const orderObj = {
			order: {
				first_name: this.props.order.firstname,
				last_name: this.props.order.lastname,
				email: this.props.order.email,
				phone: this.props.order.tel,
				delivery_company: this.props.order.deliveryWay.name,
				city: this.props.order.city,
				address: this.props.order.address,
				time: this.props.order.time,
				delivery_cost: this.props.order.deliveryWay.price,
				payment: this.props.order.paymentWay.name,
				promocode: this.props.order.promocodeValue
			},
			items: orderItems
		};
		this.props.dispatch(sendOrder(orderObj, $(this.refs['order-popup'])));
	}
}

function select(state) {
	return {
		composition: state.composition,
		basket: state.basket,
		order: state.order
	};
}

export default connect(select)(Order);
