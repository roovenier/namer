import 'whatwg-fetch';
export const SET_COMPOSITION_STATE = 'SET_COMPOSITION_STATE';
import { setDefaultNamer } from './namer';
import { setDefaultOrder } from './order';
import { HOST_URL } from '../constants';

export function setCompositionState(state) {
	return {type: SET_COMPOSITION_STATE, state};
}

export function setComposition() {
	return dispatch => {
		fetch(HOST_URL + '/shop/api/initial/')
			.then(function(response) {
				return response.json();
			})
			.then(function(data) {
				dispatch(setCompositionState(data));
				dispatch(setDefaultNamer(data.materials[0], data.pendants[0], data.baseCost, data.letterCost, data.strapLengthCost));
				dispatch(setDefaultOrder(data.deliveryWays[0], data.paymentWays[0]));
			})
	};
}
