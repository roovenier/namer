import 'whatwg-fetch';
export const SET_ORDER_FIRSTNAME = 'SET_ORDER_FIRSTNAME';
export const SET_ORDER_LASTNAME = 'SET_ORDER_LASTNAME';
export const SET_ORDER_EMAIL = 'SET_ORDER_EMAIL';
export const SET_ORDER_TEL = 'SET_ORDER_TEL';
export const SET_ORDER_CITY = 'SET_ORDER_CITY';
export const SET_ORDER_ADDRESS = 'SET_ORDER_ADDRESS';
export const SET_ORDER_TIME = 'SET_ORDER_TIME';
export const SET_PROMOCODE = 'SET_PROMOCODE';
export const SET_PROMOCODE_VALUE = 'SET_PROMOCODE_VALUE';
export const SET_DELIVERY_WAY = 'SET_DELIVERY_WAY';
export const SET_PAYMENT_WAY = 'SET_PAYMENT_WAY';
export const SET_ORDER_TOTAL_PRICE = 'SET_ORDER_TOTAL_PRICE';
export const SET_ORDER_STATE = 'SET_ORDER_STATE';
import { HOST_URL } from '../constants';

export function setOrderFirstName(firstname) {
	return {type: SET_ORDER_FIRSTNAME, firstname};
}

export function setOrderLastName(lastname) {
	return {type: SET_ORDER_LASTNAME, lastname};
}

export function setOrderEmail(email) {
	return {type: SET_ORDER_EMAIL, email};
}

export function setOrderTel(tel) {
	return {type: SET_ORDER_TEL, tel};
}

export function setOrderCity(city) {
	return {type: SET_ORDER_CITY, city};
}

export function setOrderAddress(address) {
	return {type: SET_ORDER_ADDRESS, address};
}

export function setOrderTime(time) {
	return {type: SET_ORDER_TIME, time};
}

export function setPromocode(promocode) {
	return {type: SET_PROMOCODE, promocode};
}

export function setPromocodeValue(promocode) {
	return {type: SET_PROMOCODE_VALUE, promocode};
}

export function setOrderPromocode(promocode) {
	return dispatch => {
		fetch(HOST_URL + '/shop/api/promocode/check/?code=' + promocode)
			.then(function(response) {
				return response.json()
			})
			.then(function(data) {
				dispatch(setPromocode(data));
			})
	};
}

export function setOrderPromocodeApply(promocode) {
	return dispatch => {
		dispatch(setPromocodeValue(promocode));
		dispatch(setOrderPromocode(promocode));
	};
}

export function setDeliveryWay(deliveryWay) {
	return {type: SET_DELIVERY_WAY, deliveryWay};
}

export function setPaymentWay(paymentWay) {
	return {type: SET_PAYMENT_WAY, paymentWay};
}

export function setOrderTotalPrice(price) {
	return {type: SET_ORDER_TOTAL_PRICE, price};
}

export function setOrderState(state) {
	return {type: SET_ORDER_STATE, state};
}

export function setOrder() {
	return dispatch => {
		fetch('/static/data/order.json')
			.then(function(response) {
				return response.json()
			})
			.then(function(data) {
				dispatch(setOrderState(data));
			})
	};
}

export function sendOrder(orderObj, popup) {
	return dispatch => {
		fetch(HOST_URL + '/shop/api/order/', {
				method: 'POST',
				body: JSON.stringify(orderObj),
				headers: new Headers({
					'Content-Type': 'application/json'
				})
			})
			.then(function(response) {
				return response.json()
			})
			.then(function(data) {
				if(data.result === 'success') {
					popup.remodal().open();
				}
			})
	};
}

export function setDefaultOrder(deliveryWay, paymentWay) {
	return dispatch => {
		dispatch(setDeliveryWay(deliveryWay));
		dispatch(setPaymentWay(paymentWay));
	};
}
