import 'whatwg-fetch';
export const SET_BASKET_STATE = 'SET_BASKET_STATE';
export const UPDATE_COMMON_PRICE = 'UPDATE_COMMON_PRICE';
export const ADD_TO_BASKET = 'ADD_TO_BASKET';
export const DECREASE_QUANTITY = 'DECREASE_QUANTITY';
export const INCREASE_QUANTITY = 'INCREASE_QUANTITY';

export function setBasketState(basket) {
	return {type: SET_BASKET_STATE, basket};
}

export function updateCommonPrice() {
	return {type: UPDATE_COMMON_PRICE};
}

export function addToBasket(namer) {
	return {type: ADD_TO_BASKET, namer};
}

export function decreaseQuantity(pos) {
	return {type: DECREASE_QUANTITY, pos};
}

export function increaseQuantity(pos) {
	return {type: INCREASE_QUANTITY, pos};
}

export function setBasket() {
	return dispatch => {
		fetch('/static/data/basket.json')
			.then(function(response) {
				return response.json()
			})
			.then(function(data) {
				dispatch(setBasketState(data));
			})
	};
}

export function addToBasketApply(namer) {
	return dispatch => {
		dispatch(addToBasket(namer));
		dispatch(updateCommonPrice());
	};
}

export function increaseQuantityApply(pos) {
	return dispatch => {
		dispatch(increaseQuantity(pos));
		dispatch(updateCommonPrice());
	};
}

export function decreaseQuantityApply(pos) {
	return dispatch => {
		dispatch(decreaseQuantity(pos));
		dispatch(updateCommonPrice());
	};
}
