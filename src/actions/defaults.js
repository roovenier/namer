import { setNamer } from './namer.js';
import { setOrder } from './order.js';
import { setBasket } from './basket.js';
import { setComposition } from './composition.js';

export function setDefaultAll() {
	return dispatch => Promise.all([
		dispatch(setNamer()),
		dispatch(setOrder()),
		dispatch(setComposition()),
		dispatch(setBasket())
	]);
}
