import 'whatwg-fetch';
export const SET_NAME = 'SET_NAME';
export const SET_MATERIAL = 'SET_MATERIAL';
export const SET_COLOR = 'SET_COLOR';
export const SET_LENGTH = 'SET_LENGTH';
export const SET_PENDANT = 'SET_PENDANT';
export const SET_BASE_COST = 'SET_BASE_COST';
export const SET_LETTER_COST = 'SET_LETTER_COST';
export const SET_STRAP_LENGTH_COST = 'SET_STRAP_LENGTH_COST';
export const UPDATE_PRICE = 'UPDATE_PRICE';
export const SET_NAMER_STATE = 'SET_DEFAULT_STATE';
export const SET_SHARE_LOADING = 'SET_SHARE_LOADING';
export const SET_SHARE_URL_STATE = 'SET_SHARE_URL_STATE';
export const SET_COMMENT = 'SET_COMMENT';
import { HOST_URL } from '../constants';

export function setName(name) {
	return {type: SET_NAME, name};
}

export function setMaterial(material) {
	return {type: SET_MATERIAL, material};
}

export function setColor(color) {
	return {type: SET_COLOR, color};
}

export function setLength(length) {
	return {type: SET_LENGTH, length};
}

export function setPendant(pendant) {
	return {type: SET_PENDANT, pendant};
}

export function setBaseCost(baseCost) {
	return {type: SET_BASE_COST, baseCost};
}

export function setLetterCost(letterCost) {
	return {type: SET_LETTER_COST, letterCost};
}

export function setStrapLengthCost(strapLengthCost) {
	return {type: SET_STRAP_LENGTH_COST, strapLengthCost};
}

export function updatePrice() {
	return {type: UPDATE_PRICE};
}

export function setShareLoading() {
	return {type: SET_SHARE_LOADING};
}

export function setShareUrlState(shareUrl) {
	return {type: SET_SHARE_URL_STATE, shareUrl};
}

export function setComment(comment) {
	return {type: SET_COMMENT, comment};
}

export function setShareUrl(namerObj) {
	return dispatch => {
		fetch(HOST_URL + '/shop/api/share/?namer=' + namerObj.name + '&strap_material=' + namerObj.strap_material + '&strap_material_color=' + namerObj.strap_material_color + '&strap_length=' + namerObj.strap_length + '&pendant=' + namerObj.pendant + '&comment=' + namerObj.comment)
			.then(function(response) {
				return response.json()
			})
			.then(function(data) {
				dispatch(setShareUrlState(data));
			})
	};
}

export function setNamerState(state) {
	return {type: SET_NAMER_STATE, state};
}

export function setNamer() {
	return dispatch => {
		fetch('/static/data/namer.json')
			.then(function(response) {
				return response.json()
			})
			.then(function(data) {
				dispatch(setNamerState(data));
			})
	};
}

export function setDefaultNamer(material, pendant, baseCost, letterCost, strapLengthCost) {
	return dispatch => {
		dispatch(setMaterial(material));
		dispatch(setPendant(pendant));
		dispatch(setBaseCost(baseCost));
		dispatch(setLetterCost(letterCost));
		dispatch(setStrapLengthCost(strapLengthCost));
		dispatch(updatePrice());
	};
}

export function setMaterialApply(material) {
	return dispatch => {
		dispatch(setMaterial(material));
		dispatch(updatePrice());
	};
}

export function setLengthApply(length) {
	return dispatch => {
		dispatch(setLength(length));
		dispatch(updatePrice());
	};
}

export function setPendantApply(pendant) {
	return dispatch => {
		dispatch(setPendant(pendant));
		dispatch(updatePrice());
	};
}

export function setNameApply(name) {
	return dispatch => {
		dispatch(setName(name));
		dispatch(updatePrice());
	};
}
