# coding: utf-8
from django.contrib import admin


class CommonAdmin(admin.ModelAdmin):
    readonly_fields = ('status_changed', )
