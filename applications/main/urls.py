from django.conf.urls import url
from .views import IndexView, CreateView, ShareView


urlpatterns = [
    url(r'^$', IndexView.as_view(), name='index'),
    url(r'^create/$', CreateView.as_view(), name='create'),
    url(r'^share/(?P<pk>\d+)/$', ShareView.as_view(), name='share'),
    # url(r'^exmple-slug/(?P<slug>[-\w]+)/$', 'example_list', {'template': 'example_list.html'}, name='main.example.list'),
    # url(r'^example-id/(?P<id>\d+)/$', 'example_list', {'template': 'example_list.html'}, name='main.example.list'),
]
