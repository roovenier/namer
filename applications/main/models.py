# coding: utf-8
from django.db import models

# https://django-model-utils.readthedocs.org/en/latest/fields.html#statusfield
from model_utils.models import StatusModel
from model_utils import Choices


class Common(StatusModel):
    '''
        Общий для моделей класс. Отвечает за публикацию контента.
    '''
    STATUS = Choices(
        ('published', u'опубликован'),
        ('draft', u'черновик'),
    )

    modified = models.DateTimeField(
        u'Дата последнего изменения',
        auto_now=True)

    created = models.DateTimeField(
        u'Дата создания',
        auto_now_add=True)

    class Meta:
        abstract = True
