# coding: utf-8
import json
from django.shortcuts import get_object_or_404
from django.views.generic import TemplateView
from applications.shop.models import NamerShare


class IndexView(TemplateView):
    template_name = 'index.html'


class CreateView(TemplateView):
    template_name = 'create.html'


class ShareView(TemplateView):
    template_name = 'create.html'

    def get_context_data(self, **kwargs):
        context = super(ShareView, self).get_context_data(**kwargs)
        n = get_object_or_404(NamerShare, pk=kwargs['pk'])
        context['namer'] = json.dumps({
            'namer': n.namer,
            'strap_material': {
                'id': n.strap_material.id,
                'name': n.strap_material.title,
                'price': n.strap_material.price,
            },
            'strap_material_color': {
                'id': n.strap_material_color.id,
                'name': n.strap_material_color.title,
                'hex': n.strap_material_color.color
            },
            'strap_length': n.strap_length,
            'pendant': {
                'id': n.pendant.id,
                'name': n.pendant.title,
                'price': n.pendant.price,
            },
            'comment': n.comment,
        })
        return context
