# # coding: utf-8
# from django.conf import settings
# from django.core.mail import EmailMessage
# from django.template.loader import render_to_string
# from django.db.models.signals import post_save
# from django.dispatch import receiver

# from .models import Order


# @receiver(post_save, sender=Order)
# def send_notification_mail(sender, instance, created, **kwargs):
#     if created:
#         order = instance
#         subject = u'%s, твой нэймер!' % order.first_name
#         from_email = settings.DEFAULT_FROM_EMAIL
#         to = [order.email, 'ivan@il-studio.ru', ]
#         content = render_to_string(
#             'shop/letters/order_client_check.html',
#             {'object': order})

#         msg = EmailMessage(subject, content, from_email, to)
#         msg.content_subtype = "html"
#         msg.send()
#         return msg
