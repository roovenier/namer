# coding: utf-8
from __future__ import unicode_literals

from django.apps import AppConfig


class ShopConfig(AppConfig):
    name = 'applications.shop'
    verbose_name = u'Магазин'
