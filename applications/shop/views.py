# coding: utf-8
import json
from django.shortcuts import get_object_or_404
from django.http import JsonResponse
from django.views.generic import View, TemplateView
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from django.core.mail import EmailMessage
from django.template.loader import render_to_string

from .models import (
    StrapMaterial, StrapMaterialColor, Pendant,
    Promocode, NamerShare, Order, OrderItem,
)


class CSRFExemptMixin(object):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(CSRFExemptMixin, self).dispatch(*args, **kwargs)


class InitialView(View):

    def get(self, *args, **kwargs):
        '''
            Materials block
        '''
        materials = []

        for m in StrapMaterial.published.all():
            colors = []

            for c in m.colors.all():
                colors.append({
                    'id': c.id,
                    'name': c.title,
                    'hex': c.color,
                })

            materials.append({
                'id': m.id,
                'name': m.title,
                'price': m.price,
                'colors': colors,
            })

        '''
            Pendants block
        '''
        pendants = []

        for p in Pendant.published.all():
            pendants.append({
                'id': p.id,
                'name': p.title,
                'price': p.price,
            })

        '''
            Delivery
        '''
        delivery_ways = [
            {
                'id': 1,
                'name': 'Курьер (Тюмень)',
                'value': 'courier',
                'price': 200
            },
            {
                'id': 2,
                'name': 'Почта России',
                'value': 'russianpost',
                'price': 200
            },
            {
                'id': 3,
                'name': 'EMS',
                'value': 'ems',
                'price': 800
            },
            {
                'id': 4,
                'name': 'CDEK',
                'value': 'cdek',
                'price': 60
            }
        ]

        '''
            Payments
        '''
        payment_ways = [
            {
                'id': 1,
                'name': 'Банковской картой',
                'value': 'card'
            },
            {
                'id': 2,
                'name': 'Наличные/терминал курьеру',
                'value': 'cash'
            },
            {
                'id': 3,
                'name': 'Я.Деньги',
                'value': 'yamoney'
            },
            {
                'id': 4,
                'name': 'QIWI-кошелёк',
                'value': 'qiwi'
            }
        ]

        return JsonResponse({
            'materials': materials,
            'pendants': pendants,
            'deliveryWays': delivery_ways,
            'paymentWays': payment_ways,
            'baseCost': 400,
            'letterCost': 10,
            'strapLengthCost': 5,
        })


class CheckPromocodeView(View):

    def get(self, *args, **kwargs):
        code = self.request.GET.get('code')
        qs = Promocode.objects.filter(code=code)
        promocode = qs[0] if qs else None
        error = ''

        if promocode:
            if promocode.is_active:
                return JsonResponse({
                    'result': 'success',
                    'percent': int(promocode.percent)
                })
            else:
                error = u'Данный промо-код уже не работает :('
        else:
            error = u'Неверный промо-код'

        return JsonResponse({
            'result': 'error',
            'error': error,
            'percent': 0
        })


class GetNamerShareUrl(View):

    def get(self, request, *args, **kwargs):
        namer = request.GET.get('namer')
        strap_material = request.GET.get('strap_material')
        strap_material_color = request.GET.get('strap_material_color')
        strap_length = request.GET.get('strap_length')
        pendant = request.GET.get('pendant')
        comment = request.GET.get('comment', '')

        if namer and strap_material and strap_material_color and strap_length \
           and pendant:

            try:
                sm = StrapMaterial.objects.get(id=strap_material)
                smc = StrapMaterialColor.objects.get(id=strap_material_color)
                p = Pendant.objects.get(id=pendant)
                obj, created = NamerShare.objects.get_or_create(
                    namer=namer,
                    strap_material=sm,
                    strap_material_color=smc,
                    strap_length=strap_length,
                    pendant=p,
                    comment=comment)

                return JsonResponse({
                    'result': 'success',
                    'url': obj.get_absolute_url()
                })

            except:
                return JsonResponse({
                    'result': 'error',
                    'error': u'Ошибка сохранения неймера'
                })

        return JsonResponse({
            'result': 'error',
            'error': u'Неверные ключи в запросе'
        })


class CreateOrderView(CSRFExemptMixin, View):

    def post(self, request, *args, **kwargs):
        body = json.loads(request.body)
        order_item_array = body['items']
        order = body['order']

        if 'promocode' in order:
            promocode_qs = Promocode.objects.filter(code=order['promocode'])
            order['promocode'] = promocode_qs[0] if promocode_qs else None

        o = Order.objects.create(**order)
        for item in order_item_array:
            OrderItem.objects.create(order=o, **item)

        self.send_notification_mail(o)
        return JsonResponse({
            'result': 'success',
            'orderId': o.pk
        })

    def send_notification_mail(self, order, **kwargs):
        subject = u'%s, твой нэймер!' % order.first_name
        from_email = settings.DEFAULT_FROM_EMAIL
        to = [order.email, 'ivan@il-studio.ru', ]
        content = render_to_string(
            'shop/letters/order_client_check.html',
            {'object': order})

        msg = EmailMessage(subject, content, from_email, to)
        msg.content_subtype = "html"
        msg.send()
        return msg


class PayView(TemplateView):
    template_name = 'shop/pay.html'

    def get_context_data(self, **kwargs):
        context = super(PayView, self).get_context_data(**kwargs)
        context['object'] = get_object_or_404(
            Order, payment_hash=kwargs['hash'])
        return context
