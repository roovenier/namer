# coding: utf-8
from django.conf.urls import url
from .views import InitialView, CheckPromocodeView, GetNamerShareUrl, \
    CreateOrderView, PayView


urlpatterns = [
    url(r'^api/initial/$',
        InitialView.as_view(),
        name='initial'),

    url(r'^api/promocode/check/$',
        CheckPromocodeView.as_view(),
        name='check_promocode'),

    url(r'^api/share/$',
        GetNamerShareUrl.as_view(),
        name='get_share_url'),

    url(r'^api/order/$',
        CreateOrderView.as_view(),
        name='order'),

    url(r'^pay/(?P<hash>[-\w]+)/$',
        PayView.as_view(),
        name='pay'),
]
