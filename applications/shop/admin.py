# coding: utf-8
from django.contrib import admin
from adminsortable.admin import SortableAdmin, SortableTabularInline
from applications.main.admin import CommonAdmin
from .models import (
    StrapMaterial, StrapMaterialColor, Pendant,
    Promocode, Order, OrderItem, NamerShare,
)


class StrapMaterialColorInline(SortableTabularInline):
    model = StrapMaterialColor
    extra = 1


class StrapMaterialAdmin(CommonAdmin, SortableAdmin):
    list_display = ('title', 'price', 'status', )
    list_filter = ('status', )
    inlines = [StrapMaterialColorInline, ]
admin.site.register(StrapMaterial, StrapMaterialAdmin)


class PendantAdmin(CommonAdmin, SortableAdmin):
    list_display = ('title', 'price', 'status', )
    list_filter = ('status', )
admin.site.register(Pendant, PendantAdmin)


class PromocodeAdmin(admin.ModelAdmin):
    list_display = ('code', 'is_active', 'is_one', 'percent', 'created', )
    list_filter = ('is_active', 'is_one', )
    search_fields = ('code', )
admin.site.register(Promocode, PromocodeAdmin)


class OrderItemInline(admin.StackedInline):
    model = OrderItem
    extra = 0


class OrderAdmin(admin.ModelAdmin):
    list_display = ('email', 'first_name', 'last_name', 'created', )
    inlines = [OrderItemInline, ]
    search_fields = ('email', 'first_name', 'last_name', )
    readonly_fields = ('payment_hash', )
admin.site.register(Order, OrderAdmin)


class NamerShareAdmin(admin.ModelAdmin):
    list_display = ('namer', 'strap_material', 'strap_material_color',
                    'strap_length', 'pendant', 'get_absolute_url', 'created', )
admin.site.register(NamerShare, NamerShareAdmin)
