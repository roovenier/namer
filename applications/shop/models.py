# coding: utf-8
from __future__ import unicode_literals

import uuid
from django.db import models
from django.core.urlresolvers import reverse
from adminsortable.models import SortableMixin
from adminsortable.fields import SortableForeignKey
from applications.main.models import Common


class StrapMaterial(Common, SortableMixin):
    '''
        Материал ремешка
    '''
    title = models.CharField(
        u'Название материала',
        max_length=200,
    )

    price = models.BigIntegerField(
        u'Стоимость',
        help_text=u'Числом. Без копеек.',
    )

    order = models.PositiveIntegerField(
        default=0,
        editable=False,
        db_index=True,
    )

    class Meta:
        ordering = ('order', )
        verbose_name = u'материал'
        verbose_name_plural = u'материалы ремешка'

    def __unicode__(self):
        return u'Материал: %s' % self.title


class StrapMaterialColor(SortableMixin):
    '''
        Цвета ремешков
    '''
    strap_material = SortableForeignKey(
        to=StrapMaterial,
        related_name='colors',
    )

    title = models.CharField(
        u'Цвет',
        max_length=200,
    )

    color = models.CharField(
        u'Цвет в hex',
        max_length=7,
        help_text=u'Например: #513737',
    )

    order = models.PositiveIntegerField(
        default=0,
        editable=False,
        db_index=True,
    )

    class Meta:
        ordering = ('order', )
        verbose_name = u'цвет'
        verbose_name_plural = u'цвет ремешка'

    def __unicode__(self):
        return u'Цвет: %s' % self.title


class Pendant(Common, SortableMixin):
    '''
        Подвески
    '''
    title = models.CharField(
        u'Название',
        max_length=200,
    )

    price = models.BigIntegerField(
        u'Стоимость',
        help_text=u'Числом. Без копеек.',
    )

    order = models.PositiveIntegerField(
        default=0,
        editable=False,
        db_index=True,
    )

    class Meta:
        ordering = ('order', )
        verbose_name = u'подвеску'
        verbose_name_plural = u'подвески'

    def __unicode__(self):
        return u'Подвеска: %s' % self.title


class Promocode(models.Model):
    '''
        Промо-код
    '''
    code = models.CharField(
        u'Код',
        unique=True,
        max_length=50)

    is_active = models.BooleanField(
        u'Активна?',
        default=True)

    is_one = models.BooleanField(
        u'Однаразовая?',
        default=False)

    percent = models.DecimalField(
        u'Процент скидки',
        max_digits=5,
        decimal_places=2)

    created = models.DateTimeField(
        u'Дата создания',
        auto_now_add=True)

    class Meta:
        ordering = ('-created', )
        verbose_name = u'промо-код'
        verbose_name_plural = u'промо-коды'

    def __unicode__(self):
        return u'Промо-код: %s' % self.code


def hash_generator():
    return uuid.uuid4().hex


class Order(models.Model):
    '''
        Заказ: Контакты
    '''
    first_name = models.CharField(
        u'Имя',
        max_length=50)

    last_name = models.CharField(
        u'Фамилия',
        max_length=50,
        blank=True)

    email = models.CharField(
        u'E-mail',
        max_length=50)

    phone = models.CharField(
        u'Phone',
        max_length=50)

    # TODO: Client relations
    # client = models.ForeignKey(
    #     verbose_name=u'Клиент',
    #     to='Client')

    '''
        Заказ: Доставка
    '''
    delivery_company = models.CharField(
        u'Доставка',
        max_length=50)

    city = models.CharField(
        u'Город',
        max_length=50)

    address = models.CharField(
        u'Адрес',
        max_length=50)

    time = models.CharField(
        u'Время',
        max_length=50)

    delivery_cost = models.DecimalField(
        u'Стоимость доставки',
        max_digits=10,
        decimal_places=2)

    '''
        Заказ: Оплата
    '''
    payment = models.CharField(
        u'Оплата',
        max_length=50)

    promocode = models.ForeignKey(
        verbose_name=u'Промо-код',
        to='shop.Promocode',
        blank=True,
        null=True)

    payment_hash = models.CharField(
        u'Хэш платежа',
        max_length=100,
        default=hash_generator,
        unique=True)

    '''
        Заказ: Технические моменты
    '''
    created = models.DateTimeField(
        u'Дата создания',
        auto_now_add=True)

    class Meta:
        ordering = ('-created', )
        verbose_name = u'заказ'
        verbose_name_plural = u'заказы'

    def get_total(self):
        total = self.delivery_cost
        for i in self.orderitem_set.all():
            cost = i.cost * i.quantity
            if self.promocode:
                if self.promocode.is_active:
                    cost = (cost * (100 - self.promocode.percent)) / 100
            total += cost
        return total

    def get_absolute_url(self):
        return reverse('shop:pay', kwargs={'hash': self.payment_hash})

    def __unicode__(self):
        return u'#{}, {} {}'.format(self.pk, self.last_name, self.first_name)


class OrderItem(models.Model):
    '''
        Составляющие заказа
    '''
    order = models.ForeignKey(
        to='shop.Order')

    namer = models.CharField(
        u'Нэймер',
        max_length=50)

    strap_material = models.CharField(
        u'Материал ремешка',
        max_length=50)

    strap_material_color = models.CharField(
        u'Цвет ремешка',
        max_length=50)

    strap_length = models.IntegerField(
        u'Длина ремешка')

    pendant = models.CharField(
        u'Подвеска',
        max_length=50)

    comment = models.TextField(
        u'Комментарий',
        blank=True)

    quantity = models.IntegerField(
        u'Количество',
        default=1)

    cost = models.DecimalField(
        u'Стоимость',
        max_digits=10,
        decimal_places=2)

    class Meta:
        verbose_name = u'неймер'
        verbose_name_plural = u'неймеры'

    def __unicode__(self):
        return u'{}'.format(self.namer)


class NamerShare(Common):
    '''
        Ссылки на готовые нэймеры
    '''
    namer = models.CharField(
        u'Нэймер',
        max_length=50)

    strap_material = models.ForeignKey(
        to='shop.StrapMaterial',
        verbose_name=u'Материал ремешка')

    strap_material_color = models.ForeignKey(
        to='shop.StrapMaterialColor',
        verbose_name=u'Цвет ремешка')

    strap_length = models.IntegerField(
        u'Длина ремешка')

    pendant = models.ForeignKey(
        to='shop.Pendant',
        verbose_name=u'Подвеска')

    comment = models.TextField(
        u'Комментарий',
        blank=True)

    class Meta:
        ordering = ('-created', )
        verbose_name = u'ссылку'
        verbose_name_plural = u'ссылки поделиться'

    def get_absolute_url(self):
        return 'http://dev.getnamer.ru/share/{}/'.format(self.pk)

    def __unicode__(self):
        return self.namer


import signals
