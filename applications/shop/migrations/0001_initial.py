# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-03-25 05:46
from __future__ import unicode_literals

import adminsortable.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='StrapMaterial',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=200, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u043c\u0430\u0442\u0435\u0440\u0438\u0430\u043b\u0430')),
                ('order', models.PositiveIntegerField(db_index=True, default=0, editable=False)),
            ],
            options={
                'ordering': ('order',),
                'verbose_name': '\u043c\u0430\u0442\u0435\u0440\u0438\u0430\u043b',
                'verbose_name_plural': '\u043c\u0430\u0442\u0435\u0440\u0438\u0430\u043b\u044b \u0440\u0435\u043c\u0435\u0448\u043a\u0430',
            },
        ),
        migrations.CreateModel(
            name='StrapMaterialColor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=200, verbose_name='\u0426\u0432\u0435\u0442')),
                ('color', models.CharField(help_text='\u041d\u0430\u043f\u0440\u0438\u043c\u0435\u0440: #513737', max_length=7, verbose_name='\u0426\u0432\u0435\u0442 \u0432 hex')),
                ('order', models.PositiveIntegerField(db_index=True, default=0, editable=False)),
                ('strap_material', adminsortable.fields.SortableForeignKey(on_delete=django.db.models.deletion.CASCADE, to='shop.StrapMaterial')),
            ],
            options={
                'ordering': ('order',),
                'verbose_name': '\u0446\u0432\u0435\u0442',
                'verbose_name_plural': '\u0446\u0432\u0435\u0442 \u0440\u0435\u043c\u0435\u0448\u043a\u0430',
            },
        ),
    ]
